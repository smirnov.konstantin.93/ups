VPS project
---
---
Installing
---
---
Build, create and start database

    docker-compose up -d --build

### Install project dependencies

    cd vps && poetry install

### Spawn a shell within the virtual environment

    poetry shell

---