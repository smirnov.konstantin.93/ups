from django_filters import rest_framework as filters

from vps.api.models import VpsObject


class VpsObjectFilter(filters.FilterSet):
    cpu = filters.CharFilter(field_name="cpu")
    ram = filters.CharFilter(field_name="ram")
    hdd = filters.CharFilter(field_name="hdd")
    status = filters.CharFilter(field_name="status")

    class Meta:
        model = VpsObject
        fields = ('cpu', 'ram', 'hdd', 'status',)
