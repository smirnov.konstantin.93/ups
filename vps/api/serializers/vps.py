from rest_framework import serializers

from vps.api.models import VpsObject
from .status import StatusSerializer


class VpsSerializer(serializers.ModelSerializer):
    status = StatusSerializer()

    class Meta:
        model = VpsObject
        fields = (
            'uid',
            'cpu',
            'ram',
            'hdd',
            'status'
        )


class VpsCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = VpsObject
        fields = (
            'cpu',
            'ram',
            'hdd',
            'status'
        )


class VpsStatusUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = VpsObject
        fields = (
            'status',
        )
