import uuid


class UidHelper:
    @staticmethod
    def check_uid(uid):
        if uid:
            try:
                return uuid.UUID(uid).version == 4
            except ValueError:
                return False
        else:
            return True
