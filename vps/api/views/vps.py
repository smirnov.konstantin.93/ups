from drf_yasg import openapi
from drf_yasg.utils import no_body, swagger_auto_schema
from django_filters import rest_framework as filters
from rest_framework import status
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from vps.api.models import VpsObject, Status
from vps.api.serializers import VpsSerializer, VpsCreateSerializer, VpsStatusUpdateSerializer
from vps.api.helpers.uidhelper import UidHelper
from vps.api.filters import VpsObjectFilter

uid = openapi.Parameter("uid", openapi.IN_QUERY, description="uid", type=openapi.FORMAT_UUID)


class VpsViewSet(GenericViewSet):
    model = VpsObject
    serializer_class = VpsSerializer
    queryset = VpsObject.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = VpsObjectFilter

    @swagger_auto_schema(
        operation_id="Vps create",
        operation_summary="Vps create",
        request_body=VpsCreateSerializer,
        responses={200: VpsSerializer()},
    )
    def create(self, request):
        try:
            Status.objects.get(
                id=request.data.get('status')
            )
        except Status.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"Error": "Status not found"})

        serializer = VpsCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_id="Update vps",
        operation_summary="Update vps",
        request_body=VpsStatusUpdateSerializer,
        responses={200: VpsSerializer()},
    )
    def update(self, request, pk):
        try:
            vps_obj = VpsObject.objects.filter(id=pk)
            new_status = Status.objects.get(id=request.data.get('status'))
            vps_obj.update(status=new_status)
            return Response(self.serializer_class(vps_obj,
                                                  many=True,
                                                  context={"request": request}).data,
                            status=status.HTTP_200_OK)
        except VpsObject.DoesNotExist:
            return Response(data={"Error": "Object not found"}, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_id="Get all vps",
        operation_summary="Get all vps",
        request_body=no_body,
        manual_parameters=[uid],
        responses={200: VpsSerializer()},
    )
    def list(self, request):
        if UidHelper.check_uid(request.query_params.get("uid")):
            if request.query_params.get("uid") is not None:
                self.queryset = self.queryset.filter(uid=request.query_params.get("uid"))
            return Response(self.serializer_class(self.queryset,
                                                  many=True,
                                                  context={"request": request}).data,
                            status=status.HTTP_200_OK
                            )
        else:
            return Response(data={"Error": "Not valid uid"}, status=status.HTTP_400_BAD_REQUEST)

