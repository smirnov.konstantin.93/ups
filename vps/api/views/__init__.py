from .vps import VpsViewSet
from .status import StatusViewSet

__all__ = [
    VpsViewSet,
    StatusViewSet
]
