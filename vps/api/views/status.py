from drf_yasg.utils import no_body, swagger_auto_schema
from rest_framework import status
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from vps.api.models import Status
from vps.api.serializers import StatusSerializer


class StatusViewSet(GenericViewSet):
    model = Status
    serializer_class = StatusSerializer
    queryset = Status.objects.all()

    @swagger_auto_schema(
        operation_id="Get all status",
        operation_summary="Get all status",
        request_body=no_body,
        responses={200: StatusSerializer()},
    )
    def list(self, request):
        return Response(self.serializer_class(self.queryset,
                                              many=True,
                                              context={"request": request}).data,
                        status=status.HTTP_200_OK
                        )