from django.contrib import admin

from .models import VpsObject, Status

admin.site.register(VpsObject)
admin.site.register(Status)
