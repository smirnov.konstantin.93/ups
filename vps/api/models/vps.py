import uuid
from django.db import models


class VpsObject(models.Model):
    uid = models.UUIDField()
    cpu = models.IntegerField()
    ram = models.IntegerField()
    hdd = models.IntegerField()
    status = models.ForeignKey("Status", on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        self.uid = uuid.uuid4()
        super().save(*args, **kwargs)
